# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


* 我完成的部分有
    * Basic control tools (30%) 
        – Brush and eraser
        – Color selector 
        – Simple menu (brush size) 

    * Text input (10%)
        – User can type texts on canvas 
        – Font menu (typeface and size)

    * Refresh button (10%)
        – Reset canvas 

    * Different brush shapes (15%) 
        – Circle, rectangle and triangle (5% for each shape) 

    * Download (5%)
        – Download current canvas as an image file


* 未完成部分
    * Cursor icon (10%) 
        – The image should change according to the currently used tool 

    * Un/Re -do button (10%) 

    * Image tool (5%) 
        – User can upload image and paste it 

* 特殊功能
    * button點下後會有動畫
    * 畫筆如果劃出畫布範圍，會自動停止畫筆功能，需重新在畫布上開始才可畫

    

* 之後會給範例嗎？有些東西想知道要怎麼做比較好
